import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import AuthContextProvider from './src/context/MyContext';
import AppStack from './src/stack/AppStack';

export default function App() {
  return (
    <NavigationContainer>
      <AuthContextProvider>
        <AppStack />
      </AuthContextProvider>
    </NavigationContainer>
  );
}
