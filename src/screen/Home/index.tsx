import React from 'react';
import {useContext} from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {AuthContext} from '../../context/MyContext';
import {MainScreenProps} from '../../stack/Navigation';

import {MyButton} from '../../component/MyButton';

export default function HomeScreen({
  navigation,
  route,
}: MainScreenProps<'Home'>) {
  console.log(route.params);
  const authContext = useContext(AuthContext);

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>Xin chào {authContext.userName}</Text>
      </View>
      <View style={styles.rowStyle}>
        <MyButton
          buttonText="Story"
          onPress={() =>
            navigation.navigate('Category', {
              param: 'Story',
            })
          }
        />
        <MyButton
          buttonText="Music"
          onPress={() =>
            navigation.navigate('Category', {
              param: 'Music',
            })
          }
        />
        <MyButton
          buttonText="Picture"
          onPress={() =>
            navigation.navigate('Category', {
              param: 'Picture',
            })
          }
        />
      </View>
      <View>
        <Text style={styles.title}>{route.params?.param}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 25,
  },
  rowStyle: {flexDirection: 'row'},
});
