import React from 'react';
import {MainScreenProps} from '../../stack/Navigation';
import MovieItem from '../../component/MovieItem';

export default function MovieDetail(props: MainScreenProps<'MovieDetail'>) {
  return <MovieItem movie={props.route.params} />;
}
