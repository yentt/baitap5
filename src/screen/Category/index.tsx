import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {MainScreenProps} from '../../stack/Navigation';
import {MyButton} from '../../component/MyButton';

export function Category({navigation, route}: MainScreenProps<'Category'>) {
  let pat = route?.params?.param;
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{route?.params?.param}</Text>
      <MyButton
        buttonText="Come Back Home"
        onPress={() => navigation.navigate('Home', {success: true, param: pat})}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 25,
  },
});
