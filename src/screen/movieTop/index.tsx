import React from 'react';
import {useEffect, useState} from 'react';
import ShowMovieList from '../../component/ShowMovieList';
import {RootObject} from '../../model/Movie';
export interface Props {}

const MovieTop: React.FC<Props> = () => {
  const [isLoading, setLoading] = useState(false);
  const [postJson, setPostJson] = useState<RootObject>();
  useEffect(() => {
    setLoading(true);
    fetch(
      'https://api.themoviedb.org/3/movie/top_rated?api_key=6a6ed66f6b27325d9b4e1a379dd84be2&language=en-US&page=1',
    )
      .then(response => response.json())
      .then(json => setPostJson(json))
      .finally(() => setLoading(false));
  }, []);
  return <ShowMovieList loading={isLoading} data={postJson?.results ?? []} />;
};

export default MovieTop;
