import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MovieTab from '../stack/MovieStack';
import HomeTab from './MainTabStack';

const Drawer = createDrawerNavigator();

export default function MainDrawer() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        options={{
          drawerIcon: () => <Icon name="home" color="black" size={25} />,
        }}
        name="HomeTab"
        component={HomeTab}
      />
      <Drawer.Screen
        options={{
          drawerIcon: () => <Icon name="theaters" color="black" size={25} />,
        }}
        name="Movie List"
        component={MovieTab}
      />
    </Drawer.Navigator>
  );
}
