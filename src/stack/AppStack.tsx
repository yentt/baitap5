import * as React from 'react';
import {useContext} from 'react';
import {AuthContext} from '../context/MyContext';
import {Login} from '../screen/Login';
import MainDrawer from './MainDraw';

const AppStack = () => {
  const authContext = useContext(AuthContext);
  return <>{authContext.isAuth ? <MainDrawer /> : <Login />}</>;
};
export default AppStack;
