import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {RootStackParamList} from './Navigation';

import Icon from 'react-native-vector-icons/MaterialIcons';
import {createStackNavigator} from '@react-navigation/stack';
import MovieList from '../screen/movieList';
import MovieTop from '../screen/movieTop';
import MovieDetail from '../screen/movieDetail';

const Stack = createStackNavigator<RootStackParamList>();

export default function MovieTab() {
  return (
    <Stack.Navigator initialRouteName="MovieTab">
      <Stack.Screen name="ListTab" component={ListTab} />
      <Stack.Screen name="MovieDetail" component={MovieDetail} />
    </Stack.Navigator>
  );
}

const Tab = createBottomTabNavigator();

const ListTab = () => {
  return (
    <Tab.Navigator
      initialRouteName="Popular"
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen
        options={{
          tabBarIcon: () => <Icon name="class" color="black" size={25} />,
        }}
        name="Popular"
        component={MovieList}
      />
      <Tab.Screen
        options={{
          tabBarIcon: () => <Icon name="verified" color="black" size={25} />,
        }}
        name="Top"
        component={MovieTop}
      />
    </Tab.Navigator>
  );
};
