import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {Movie} from '../model/Movie';

export type RootStackParamList = {
  HomeTab: undefined;
  MainTab: undefined;
  ListTab: undefined;
  MovieTab: undefined;
  Home: {success: boolean; param: any};
  About: {success: boolean} | undefined;
  Category: {param: string} | undefined;
  MovieDetail: Movie;
};
type S = keyof RootStackParamList;
export type MainStackNavigation = StackNavigationProp<RootStackParamList>;
export type RootScreenRouteProp<ScreenName extends S> = RouteProp<
  RootStackParamList,
  ScreenName
>;
export type RootScreenNavigationProp<ScreenName extends S> =
  StackNavigationProp<RootStackParamList, ScreenName>;

export type MainScreenProps<ScreenName extends S> = {
  route: RootScreenRouteProp<ScreenName>;
  navigation: RootScreenNavigationProp<ScreenName>;
};
