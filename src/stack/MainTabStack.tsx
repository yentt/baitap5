import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
//import HomeScreen from '../screen/Home';
import {RootStackParamList} from './Navigation';

import Icon from 'react-native-vector-icons/MaterialIcons';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screen/Home';
import About from '../screen/About';
import {Category} from '../screen/Category';

const Stack = createStackNavigator<RootStackParamList>();

export default function HomeTab() {
  return (
    <Stack.Navigator initialRouteName="MainTab">
      <Stack.Screen name="MainTab" component={MainTab} />
      <Stack.Screen name="Category" component={Category} />
    </Stack.Navigator>
  );
}

const Tab = createBottomTabNavigator();

const MainTab = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen
        options={{
          tabBarIcon: () => <Icon name="home" color="black" size={25} />,
        }}
        name="Home"
        component={HomeScreen}
      />
      <Tab.Screen
        options={{
          tabBarIcon: () => <Icon name="info" color="black" size={25} />,
        }}
        name="About"
        component={About}
      />
    </Tab.Navigator>
  );
};
