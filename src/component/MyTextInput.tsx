import React, {Fragment, RefAttributes} from 'react';
import {StyleSheet, Text, TextInput, TextInputProps} from 'react-native';

export type Props = TextInputProps &
  RefAttributes<TextInput> & {
    error?: string;
  };

export const MyTextInput: React.FC<Props> = React.forwardRef<TextInput, Props>(
  ({error, ...others}: Props, ref) => {
    return (
      <Fragment>
        <TextInput ref={ref} {...others} />
        {error && <Text style={styles.error}>{error}</Text>}
      </Fragment>
    );
  },
);

const styles = StyleSheet.create({
  error: {
    marginHorizontal: 15,
    color: 'red',
  },
});

export default MyTextInput;
