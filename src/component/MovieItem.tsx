// import React from 'react';
// import {useCallback} from 'react';
// import {useNavigation} from '@react-navigation/native';
// import {
//   View,
//   Text,
//   StyleSheet,
//   ActivityIndicator,
//   FlatList,
//   TouchableOpacity,
//   Image,
// } from 'react-native';

// import {Movie} from '../model/Movie';
// import {MainStackNavigation} from '../stack/Navigation';
// import MovieItem from './MovieItem';

// export interface Props {
//   loading: boolean;
//   data: Movie[];
// }

// export default function ShowMovieList({loading, data}: Props) {
//   const mainStackNavigation = useNavigation<MainStackNavigation>();
//   const onItemClick = useCallback(
//     (movie: Movie) => {
//       mainStackNavigation.navigate('MovieDetail', movie);
//     },
//     [mainStackNavigation],
// );
//   let url = 'https://image.tmdb.org/t/p/w500';
//   const keyExtractor = (item: Movie, index: any) => index.toString();
//   const renderItem = useCallback(
//     ({item}: any) => (
//       <TouchableOpacity onPress={() => {}}>
//         <View style={styles.row}>
//           <Image
//             style={styles.thumb}
//             source={{
//             uri: url + item.poster_path,
//             }}
//           />
//           <Text style={styles.text}>{item.title}</Text>
//         </View>
//       </TouchableOpacity>
//     ), [onItemClick],
// );
// ;

//   return (
//     <View style={styles.container}>
//       <Text style={styles.title}>Movie List</Text>
//       {loading ? (
//         <ActivityIndicator color="red" size="large" />
//       ) : (
//         <FlatList
//           data={data}
//           keyExtractor={keyExtractor}
//           renderItem={renderItem}
//         />
//       )}
//     </View>
//   );
// };
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   title: {
//     color: 'green',
//     fontWeight: 'bold',
//     fontSize: 25,
//   },
//   row: {
//     flexDirection: 'row',
//     padding: 10,
//     backgroundColor: '#F6F6F6',
//   },

//   thumb: {
//     width: 100,
//     height: 100,
//   },

//   text: {
//     padding: 10,
//   },
// });
import React, {useCallback} from 'react';
import {TouchableOpacity, Text, StyleSheet, Image, View} from 'react-native';
import {Movie} from '../model/Movie';

type MovieItemProps = {
  movie: Movie;
  onPress?: (movie: Movie) => void;
};

const MovieItem: React.FC<MovieItemProps> = ({movie, onPress}) => {
  const localClick = useCallback(() => {
    onPress?.(movie);
  }, [movie, onPress]);
  let url = 'https://image.tmdb.org/t/p/w500';
  return (
    <TouchableOpacity onPress={localClick}>
      <View style={styles.row}>
        <Image
          style={styles.thumb}
          source={{
            uri: url + movie.poster_path,
          }}
        />
        <Text style={styles.text}>{movie.title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default React.memo(MovieItem);

const styles = StyleSheet.create({
  item: {
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    flexDirection: 'row',
  },
  image: {
    width: 120,
    height: 120,
  },
  itemText: {
    fontSize: 14,
  },
  title: {
    fontWeight: '800',
    fontSize: 20,
  },
  rightContent: {
    flex: 1,
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#F6F6F6',
  },
  thumb: {
    width: 100,
    height: 100,
  },
  text: {
    padding: 10,
  },
});
