import React from 'react';
import {useCallback} from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  View,
  Text,
  ActivityIndicator,
  FlatList,
  StyleSheet,
} from 'react-native';

import {Movie} from '../model/Movie';
import {MainStackNavigation} from '../stack/Navigation';
import MovieItem from './MovieItem';

export interface Props {
  loading: boolean;
  data: Movie[];
}

export default function ShowMovieList({loading, data}: Props) {
  const mainStackNavigation = useNavigation<MainStackNavigation>();
  const onItemClick = useCallback(
    (movie: Movie) => {
      mainStackNavigation.navigate('MovieDetail', movie);
    },
    [mainStackNavigation],
  );
  const keyExtractor = (item: Movie, index: any) => index.toString();
  const renderItem = useCallback(
    ({item}: any) => <MovieItem movie={item} onPress={onItemClick} />,
    [onItemClick],
  );

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Movie List</Text>
      {loading ? (
        <ActivityIndicator color="red" size="large" />
      ) : (
        <FlatList
          data={data}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 25,
  },
});
