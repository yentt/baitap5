// import {createContext, useContext} from 'react';
// export type GlobalContent = {
//   copy: string;
//   setCopy: (c: string) => void;
//   data: any;
//   setData: (d: string) => void;
// };
// export const MyGlobalContext = createContext<GlobalContent>({
//   copy: 'userName', // set a default value
//   setCopy: () => {},
//   data: 'data', // set a default value
//   setData: () => {},
// });
// export const useGlobalContext = () => useContext(MyGlobalContext);
import React, {useMemo, useState} from 'react';
import {PropsWithChildren} from 'react';

export type AuthContextProps = {
  isAuth: boolean;
  setAuth?: (isAuth: boolean) => void;
  userName?: string;
  setUserName?: (userName?: string) => void;
};

export const AuthContext = React.createContext<AuthContextProps>({
  isAuth: false,
});

const AuthContextProvider = (props: PropsWithChildren<any>) => {
  const [isAuth, setAuth] = useState(false);
  const [userName, setUserName] = useState<string>();
  const authValue: AuthContextProps = useMemo(
    () => ({
      isAuth,
      setAuth,
      userName,
      setUserName,
    }),
    [isAuth, userName],
  );
  return (
    <AuthContext.Provider value={authValue}>
      {props.children}
    </AuthContext.Provider>
  );
};
export default AuthContextProvider;
